<?php
  session_start();
  if(isset($_GET['action'])){
    if($_GET['action']=='clear'){
      session_unset();
      session_destroy();
    }elseif($_GET['action']=='delete'){
      unset($_SESSION['bookmark'][$_GET['name']]);
      header('location:index.php');
    }
  }
  if(isset($_POST['webname'])){
    if($_POST['webname']==''|| $_POST['weburl']==''){
      header('location:index.php?error=Please%20enter%20Correctly');
    }else{
      if(isset($_SESSION['bookmark'])){
        $_SESSION['bookmark'][$_POST['webname']]=$_POST['weburl'];
      }else{
        $_SESSION['bookmark']=Array($_POST['webname'] => $_POST['weburl']);
      }
    }
  }

?>

<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- fontawsome css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cyborg/bootstrap.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
      <a class="navbar-brand" href="#">Bookmarker</a>
      <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
          aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
      <div class="collapse navbar-collapse navabar-right" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="index.php?action=clear">clear<span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container">
      <?php if(isset($_GET['error'])): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <i class="fa fa-warning" aria-hidden="true"></i>
          <strong><?php echo $_GET['error'] ?></strong> 
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-md-7">
          <form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
              <label for="webname">Website Name</label>
              <input type="text" class="form-control" name="webname" id="webname" aria-describedby="helpId" placeholder="url">
              <small id="helpId" class="form-text text-muted">Please Enter a suitable name for the site</small>
            </div>
            <div class="form-group">
              <label for="weburl">URL</label>
              <input type="url"
                class="form-control" name="weburl" id="weburl" aria-describedby="helpId" placeholder="url">
              <small id="helpId" class="form-text text-muted">Correct url must be given</small>
            </div>
            <input type="submit" value="Submit" class="btn btn-success">
          </form>
        </div>
        <div class="col-md-5">
          <h4>Bookmarks:</h4>
          <ul>
          <?php if(isset($_SESSION['bookmark'])): ?>
            <?php foreach($_SESSION['bookmark'] as $name => $url): ?>
              <li>
                <a target='blank' href="<?php echo $url; ?>"><?php echo $name; ?></a>
                <a href="index.php?action=delete&name=<?php echo $name ?>"><i class="fa fa-close" aria-hidden="true"></i></a>
              </li>
            <?php endforeach; ?>
          <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script>
    $(".alert").alert();
  </script>
    </body>
</html>

