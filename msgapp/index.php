<?php
    include 'db.php';
    $sql ="SELECT * FROM massages ORDER BY create_date DESC";
    $msg = mysqli_query($connect,$sql);
    //print_r($msg);
    if(isset($_GET['error'])){
        $error=$_GET['error'];
    }

    if(isset($_GET['action'])){
        if($_GET['action']=='alert' && isset($_GET['alert'])){
            $alert=$_GET['alert'];
            $showmsg=$_GET['showmsg'];
        }else if($_GET['action']=='delete' && isset($_GET['id'])){
            $delid= $_GET['id'];
            $sql="DELETE FROM massages WHERE id=$delid";
            if(!mysqli_query($connect,$sql)){
                die(mysqli_error($connect));
            }else{
                header("Location: index.php?action=alert&alert=danger&showmsg=Massage%20Removed");
            }
        }
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Msgapp</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  </head>
  <body>
    <header>
       <div class="container mt-5">
         <h1><i class="fa fa-comment-o" aria-hidden="true"></i> Massage App</h1>
         <h5><i class="fa fa-user-circle-o" aria-hidden="true"></i> A vulnarable site made by PrinceBilly</h5>
       </div>
    </header>
    <main>
        <div class="container">
            <form method="POST" action="process.php" class="pb-3">
                <div class="form-group">
                <label for="msg">Enter you text here</label>
                <textarea class="form-control" name="text" id="msg" rows="3" maxlength="255" placeholder="Enter you msg here.."></textarea>
                </div>
                <div class="form-group">
                <input type="text" class="form-control" name="host" id="user" aria-describedby="username" placeholder="username">
                <small id="host" class="form-text text-muted">Enter your userid</small>
                </div>
                <button type="submit" class="btn btn-success">Send</button>
            </form>
            <?php if(isset($error)): ?>
            <div class="alert alert-danger" role="alert">
                <i class="fa fa-warning" aria-hidden="true"></i> <strong><?php echo $error ?></strong>
            </div>
            <?php endif; ?>
            <?php while($row = mysqli_fetch_assoc($msg)): ?>
                <div class="jumbotron p-3">
                    <h5 class="float-left">Massage <?php echo $row['id'] ?></h5>
                    <a href="index.php?action=delete&id=<?php echo $row['id'] ?>" type="button" class="float-right btn btn-danger"><i class="text-light fa fa-trash-o" aria-hidden="true"></i></a>
                    <div class="clearfix"></div>
                    <h6><?php echo $row['create_date'] ?></h6>
                    <hr class="my-2">
                    <p><?php echo $row['text'] ?></p>
                    <p class="lead">
                        <p class="text-primary">
                        <?php echo $row['user'] ?>
                        </p>
                    </p>
                </div>
            <?php endwhile; ?>
        </div>

    </main>

    <?php if(isset($alert)):  ?>
        <div id="submited" class="fixed-bottom alert alert-<?php echo $alert ?>" role="alert">
            <strong><?php echo $showmsg ?></strong>
        </div>
    <?php endif; ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <?php if(isset($alert)):  ?>   
        <script>
            $(document).ready(function () {
                $('#submited').fadeOut(2000);
            });
        </script>
     <?php endif; ?>
  </body>
</html>